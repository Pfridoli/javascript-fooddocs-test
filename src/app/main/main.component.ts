import {Component, OnInit} from '@angular/core';
import {Person} from '../Person';
import {DataService} from '../data.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: []
})
export class MainComponent implements OnInit {

  constructor(private dataService: DataService) {
    this.getData();
  }

  persons: Person[];

  // Gets data from Person table
  getData() {
    this.dataService.getPersons().then(persons => this.persons = persons);
  }

  // Sets color for age
  getAgeColor(test) {
    return test >= 18 ? 'green' : 'red';
  }

  // Deletes person from the table (doesn't effect database)
  deletePerson(id) {
    this.persons.splice(id, 1);
  }

  ngOnInit() {
    this.getData();
  }

}
