import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Person} from './Person';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) {
  }

  // Gets data from Person table with GET method
  public getPersons(): Promise<Person[]> {
    return this.http.get('http://dev.api.fooddocs.ee/testtask')
      .toPromise()
      .then(result => <Person[]> result);
  }

}
